import 'reflect-metadata'

import request from 'supertest'

import { TestContext } from './context/TestContext'

describe('Health check route', () => {
  beforeAll(async done => {
    await TestContext.initialize()
    done()
  })

  test('ping route', async () => {
    await TestContext.initialize()

    const response = await request(TestContext.app).get('/')

    expect(response.status).toBe(200)
    expect(response.body.message).toBe('Express Server Skeleton!')

    await TestContext.close()
  })

  afterAll(async done => {
    await TestContext.close()
    done()
  })
})
