import 'reflect-metadata'

import { List } from 'immutable'
import request from 'supertest'

import { PagedList } from '../../../../../src/skeleton/generic/model/PagedList'
import { User } from '../../../../../src/skeleton/user/model/User'
import { usersList } from '../../../../database/seeds/01_InitialData'
import {
  administratorData,
  facebookUserData,
  googleUserData,
  journalistData,
  moderatorData,
  publicUserData,
  regularUserData,
  superuserData,
} from '../../../common/TestUtilities'
import { TestContext } from '../../../context/TestContext'

describe('UserController', () => {
  beforeAll(async done => {
    await TestContext.initialize()
    done()
  })

  test('users list without auth token', async () => {
    // GIVEN no jwt token
    // WHEN users list is requested
    const response = await request(TestContext.app).get('/users')

    // THEN response is successful
    expect(response.status).toBe(403)
    expect(response.body.message).toBe('Access denied')
  })

  test('users list successful', async () => {
    // GIVEN superuser jwt token
    const expectResponse: PagedList<User> = {
      metadata: { limit: 100, offset: 0, total: 8 },
      list: List([
        superuserData,
        administratorData,
        journalistData,
        moderatorData,
        publicUserData,
        regularUserData,
      ]),
    }
    // WHEN users list is requested
    const response = await request(TestContext.app)
      .get('/users')
      .set('Authorization', superuserData.token)

    // THEN response is successful
    expect(response.status).toBe(200)

    // AND the result should contain 8 users
    expect(response.body.metadata).toStrictEqual(expectResponse.metadata)
    expect(response.body.list.length).toBe(8)

    const responseUsersList: Array<User> = response.body.list
    const sortedUsersList: Array<User> = responseUsersList.sort((a: User, b: User) =>
      (a.uid as string).localeCompare(b.uid as string),
    )

    expect(sortedUsersList[0]).toMatchObject({
      uid: superuserData.uid,
      username: superuserData.username,
      role: superuserData.role,
    })
    expect(sortedUsersList[1]).toMatchObject({
      uid: administratorData.uid,
      username: administratorData.username,
      role: administratorData.role,
    })
    expect(sortedUsersList[2]).toMatchObject({
      uid: journalistData.uid,
      username: journalistData.username,
      role: journalistData.role,
    })
    expect(sortedUsersList[3]).toMatchObject({
      uid: moderatorData.uid,
      username: moderatorData.username,
      role: moderatorData.role,
    })
    expect(sortedUsersList[4]).toMatchObject({
      uid: publicUserData.uid,
      username: publicUserData.username,
      role: publicUserData.role,
    })
    expect(sortedUsersList[5]).toMatchObject({
      uid: regularUserData.uid,
      username: regularUserData.username,
      role: regularUserData.role,
    })
    expect(sortedUsersList[6]).toMatchObject({
      uid: facebookUserData.uid,
      username: facebookUserData.username,
      role: facebookUserData.role,
    })
    expect(sortedUsersList[7]).toMatchObject({
      uid: googleUserData.uid,
      username: googleUserData.username,
      role: googleUserData.role,
    })
  })

  test('POST to /users successfully', async () => {
    // GIVEN application, superuser credentials and new user data
    const userData = {
      username: 'additional.user@gmail.com',
      password: 'test4now!',
    }

    // WHEN POST to /users is done
    const response = await request(TestContext.app)
      .post('/users')
      .set('Authorization', superuserData.token)
      .send(userData)

    // THEN response must be Created
    expect(response.status).toBe(201)

    // AND body should contain new user's uid
    expect(typeof response.body.uid).toBe('string')
    expect(response.body.uid).toHaveLength(36)
  })

  test('GET to /users/:userId successfully', async () => {
    // GIVEN application, superuser credentials and user data
    // WHEN request is done to /users/:userId address
    const response = await request(TestContext.app)
      .get(`/users/${usersList[1].uid}`)
      .set('Authorization', superuserData.token)

    // THEN response must be successful
    expect(response.status).toBe(200)

    // AND body should contain user's data
    expect(response.body).toStrictEqual({
      ...usersList[1],
      createdAt: null,
      deletedAt: null,
      lastLoginAt: null,
      password: null,
    })
  })

  test('GET to /users/:userId successfully', async () => {
    // GIVEN application, superuser credentials and user data
    // WHEN request is done to /users/:userId address
    const response = await request(TestContext.app)
      .get(`/users/00000000-0000-0000-0000-000000000000`)
      .set('Authorization', superuserData.token)

    // THEN response must be successful
    expect(response.status).toBe(404)
  })

  test('PUT to /users/:userId successfully', async () => {
    // GIVEN application, superuser credentials and a user data to be updated
    const userData = {
      username: 'updated.user@gmail.com',
      password: 'test4now!',
    }

    // WHEN request to /users/:userId is done
    const response = await request(TestContext.app)
      .put(`/users/${usersList[7].uid}`)
      .set('Authorization', superuserData.token)
      .send(userData)

    // THEN response must be successful with no content
    expect(response.status).toBe(204)
    // AND body should be empty
    expect(response.body).toStrictEqual({})
  })

  test('DELETE to /users/:userId successfully', async () => {
    // GIVEN application, superuser credentials and user uid to be deleted
    // WHEN DELETE to /users/:userId is done
    const response = await request(TestContext.app)
      .delete(`/users/${usersList[4].uid}`)
      .set('Authorization', superuserData.token)

    // THEN response must be successful with no content
    expect(response.status).toBe(204)
    // AND body should be empty
    expect(response.body).toStrictEqual({})

    // AND search by user's uid should return nothing
    const deleteTest = await request(TestContext.app)
      .get(`/users/${usersList[4].uid}`)
      .set('Authorization', superuserData.token)
    expect(deleteTest.status).toBe(404)
  })

  afterAll(async done => {
    await TestContext.close()
    done()
  })
})
