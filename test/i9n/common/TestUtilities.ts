export const superuserData = {
  uid: '00000000-aaaa-aaaa-aaaa-000000000001',
  username: 'alpha.admin@gmail.com',
  password: 'test4now!',
  role: 'superuser',
  token: 'non-secret-access',
}

export const administratorData = {
  uid: '00000000-aaaa-aaaa-aaaa-000000000002',
  username: 'beta.user@gmail.com',
  password: 'test4now!',
  role: 'administrator',
}

export const journalistData = {
  uid: '00000000-aaaa-aaaa-aaaa-000000000003',
  username: 'gamma.user@gmail.com',
  password: 'test4now!',
  role: 'journalist',
}

export const moderatorData = {
  uid: '00000000-aaaa-aaaa-aaaa-000000000004',
  username: 'delta.user@gmail.com',
  password: 'test4now!',
  role: 'moderator',
}

export const publicUserData = {
  uid: '00000000-aaaa-aaaa-aaaa-000000000005',
  username: 'epsilon.user@gmail.com',
  password: 'test4now!',
  role: 'user',
}

export const regularUserData = {
  uid: '00000000-aaaa-aaaa-aaaa-000000000006',
  username: 'zeta.user@gmail.com',
  password: 'test4now!',
  role: 'user',
}

export const facebookUserData = {
  uid: '00000000-aaaa-aaaa-aaaa-000000000007',
  username: '100004290272604@facebook',
  role: 'user',
}

export const googleUserData = {
  uid: '00000000-aaaa-aaaa-aaaa-000000000008',
  username: '111111111111111@google',
  role: 'user',
}
