import * as Knex from 'knex'
import { container } from 'tsyringe'

import { logger } from '../../src/skeleton/logger/LoggerFactory'

class TestDBModule {
  static async initialize(seeds?: Array<(knex: Knex) => Promise<void>>): Promise<void> {
    const knex = container.resolve<Knex>('DBConnection')
    await TestDBModule.initializeTestDB(knex)

    if (seeds) {
      await Promise.all(seeds.map(seed => seed(knex)))
    }

    logger.debug('test.context.database.initialized')
  }

  static async close(): Promise<void> {
    const knex = container.resolve<Knex>('DBConnection')

    await knex.destroy()

    logger.debug('test.context.database.closed')
  }

  private static async dropData(knex: Knex): Promise<void> {
    await knex.raw('DROP SCHEMA public CASCADE;')
    await knex.raw('CREATE SCHEMA public;')
  }

  private static async initializeTestDB(knex: Knex): Promise<void> {
    await TestDBModule.dropData(knex)
    await knex.migrate.latest()
  }
}

export default TestDBModule
