import * as Knex from 'knex'

import { User } from '../../../src/skeleton/user/model/User'
import {
  administratorData,
  facebookUserData,
  googleUserData,
  journalistData,
  moderatorData,
  publicUserData,
  regularUserData,
  superuserData,
} from '../../i9n/common/TestUtilities'

export const usersList: Array<User> = [
  {
    uid: superuserData.uid,
    username: superuserData.username,
    role: superuserData.role,
  },
  {
    uid: administratorData.uid,
    username: administratorData.username,
    role: administratorData.role,
  },
  {
    uid: journalistData.uid,
    username: journalistData.username,
    role: journalistData.role,
  },
  {
    uid: moderatorData.uid,
    username: moderatorData.username,
    role: moderatorData.role,
  },
  {
    uid: publicUserData.uid,
    username: publicUserData.username,
    role: publicUserData.role,
  },
  {
    uid: regularUserData.uid,
    username: regularUserData.username,
    role: regularUserData.role,
  },
  {
    uid: facebookUserData.uid,
    username: facebookUserData.username,
    role: facebookUserData.role,
  },
  {
    uid: googleUserData.uid,
    username: googleUserData.username,
    role: googleUserData.role,
  },
]

export async function seed(knex: Knex): Promise<void> {
  await knex('user').insert(usersList)
}
