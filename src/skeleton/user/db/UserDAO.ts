import { TrxProvider } from '../../db/TrxProvider'
import { GenericDAO } from '../../generic/dao/GenericDAO'
import { ACS } from '../../security/acs/models/ACS'
import { User } from '../model/User'

export interface UserDAO extends GenericDAO<User> {
  findUser(trxProvider: TrxProvider, username: string): Promise<User | undefined>

  updateUserLastLogin(trxProvider: TrxProvider, uid: string): Promise<void>

  updateUsername(trxProvider: TrxProvider, username: string, acs: ACS): Promise<void>
}
