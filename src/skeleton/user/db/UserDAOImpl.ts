import { List } from 'immutable'
import { injectable } from 'tsyringe'
import uuidv4 from 'uuid/v4'

import { DateUtility } from '../../common/utils/DateUtility'
import { TrxProvider } from '../../db/TrxProvider'
import { checkDAOResult } from '../../generic/dao/ErrorsDAO'
import { EntityFilter } from '../../generic/model/EntityFilter'
import { PagedList } from '../../generic/model/PagedList'
import { PaginationMetadata } from '../../generic/model/PaginationMetadata'
import { PaginationUtility } from '../../generic/utils/PaginationUtility'
import { logger } from '../../logger/LoggerFactory'
import { ACS } from '../../security/acs/models/ACS'
import { User } from '../model/User'
import { UserDAO } from './UserDAO'

@injectable()
export class UserDAOImpl implements UserDAO {
  public saveOrUpdate(trxProvider: TrxProvider, entity: User): Promise<string> {
    if (entity.uid) {
      return this.update(trxProvider, entity).then(() => entity.uid as string)
    } else {
      return this.create(trxProvider, entity)
    }
  }

  private async create(trxProvider: TrxProvider, user: User): Promise<string> {
    logger.debug('user.dao.create.start')
    const uid = uuidv4()

    const trx = await trxProvider()
    await trx('user').insert({
      uid: uid,
      username: user.username,
      password: user.password,
      role: user.role,
      createdAt: DateUtility.now(),
    })

    logger.debug('user.dao.create.done')
    return uid
  }

  private async update(trxProvider: TrxProvider, user: User): Promise<void> {
    logger.debug('user.dao.update.start')
    const trx = await trxProvider()

    const result = await trx('user')
      .where({ uid: user.uid })
      .update({
        username: user.username,
        password: user.password,
        role: user.role,
      })

    checkDAOResult(result, 'user', 'update')
    logger.debug('user.dao.update.done')
  }

  public async get(trxProvider: TrxProvider, uid: string, acs: ACS): Promise<User | undefined> {
    logger.debug('user.dao.get.start')
    const trx = await trxProvider()

    const result = await trx<User | undefined>('user')
      .where({ uid: uid })
      .andWhere(acs.toSQL('uid'))
      .whereNull('deletedAt')
      .first()

    logger.debug('user.dao.get.start')
    return result
  }

  public async delete(trxProvider: TrxProvider, uid: string): Promise<void> {
    logger.debug('user.dao.delete.start')
    const trx = await trxProvider()

    const result = await trx('user')
      .where({ uid: uid })
      .update({
        role: 'deleted',
        deletedAt: DateUtility.now(),
      })

    checkDAOResult(result, 'user', 'delete')
    logger.debug('user.dao.delete.done')
  }

  public async findUser(trxProvider: TrxProvider, username: string): Promise<User | undefined> {
    logger.debug('user.dao.find-user.start')
    const trx = await trxProvider()

    const result = await trx<User | undefined>('user')
      .where({ username: username })
      .whereNull('deletedAt')
      .first()

    logger.debug('user.dao.find-user.start')
    return result
  }

  public async updateUserLastLogin(trxProvider: TrxProvider, uid: string): Promise<void> {
    logger.debug('user.dao.last-login-upd.start')
    const trx = await trxProvider()

    logger.debug('user.dao.last-login-upd.query.start')
    const result = await trx('user')
      .where({ uid: uid })
      .update({
        lastLoginAt: DateUtility.now(),
      })

    checkDAOResult(result, 'user', 'last-login-upd')
    logger.debug('user.dao.last-login-upd.done')
  }

  public async list(
    trxProvider: TrxProvider,
    filter: EntityFilter,
    acs: ACS,
  ): Promise<PagedList<User>> {
    logger.debug('user.dao.list.start')
    const trx = await trxProvider()

    const mainQuery = trx<User>('user')
      .where(acs.toSQL('uid'))
      .whereNull('deletedAt')

    const pageMetadata: PaginationMetadata = await PaginationUtility.calculatePaginationMetadata(
      mainQuery,
      filter,
    )
    logger.debug('user.dao.list.counted')

    const users = await PaginationUtility.applyPaginationForQuery(mainQuery, filter).select('*')

    logger.debug('user.dao.list.done')
    return {
      metadata: pageMetadata,
      list: List(users),
    }
  }

  public async updateUsername(trxProvider: TrxProvider, username: string, acs: ACS): Promise<void> {
    logger.debug('user.dao.username-upd.start')
    const trx = await trxProvider()

    logger.debug('user.dao.username-upd.query.start')
    const result = await trx('user')
      .where(acs.toSQL('uid'))
      .update({ username })

    checkDAOResult(result, 'user', 'username-upd')
    logger.debug('user.dao.username-upd.done')
  }
}
