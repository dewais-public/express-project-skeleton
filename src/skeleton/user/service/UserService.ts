import { GenericService } from '../../generic/service/GenericService'
import { User } from '../model/User'

export interface UserService extends GenericService<User> {
  findUser(username: string): Promise<User | undefined>

  updateUserLastLogin(username: string): Promise<void>
}
