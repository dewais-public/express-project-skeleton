import joi from '@hapi/joi'

import {
  adaptValidationResult,
  ValidationResult,
  Validator,
} from '../../common/validators/Validator'
import { User } from '../model/User'

export class UserValidator implements Validator<User> {
  public joiValidator = joi.object({
    username: joi
      .string()
      .min(3)
      .required(),
    password: joi
      .string()
      .min(8)
      .required(),
    uid: joi.string().uuid(),
    role: joi
      .string()
      .max(6)
      .required(),
  })

  validate(modelObject: User): ValidationResult {
    return adaptValidationResult(this.joiValidator.validate(modelObject))
  }
}
