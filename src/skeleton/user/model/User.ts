import { GenericEntity } from '../../generic/model/GenericEntity'

export interface User extends GenericEntity {
  readonly username: string
  readonly password?: string
  readonly role: string
  readonly createdAt?: Date
  readonly lastLoginAt?: Date
}
