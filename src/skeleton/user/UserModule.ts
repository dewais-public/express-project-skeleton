import { container } from 'tsyringe'

import { logger } from '../logger/LoggerFactory'
import { UserDAOImpl } from './db/UserDAOImpl'
import { UserServiceImpl } from './service/UserServiceImpl'

export class UserModule {
  static async initialize(): Promise<void> {
    container.registerSingleton('UserDAO', UserDAOImpl)
    container.registerSingleton('UserService', UserServiceImpl)
    logger.debug('app.context.user.module.initialized')
  }
}
