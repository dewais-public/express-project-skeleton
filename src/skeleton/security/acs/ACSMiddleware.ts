import { NextFunction, Request, Response } from 'express'
import * as HttpStatus from 'http-status-codes'

import { ServerError } from '../../error/ServerError'
import { GrandAccessACS } from './strategies'

export const verifyAccess = (...currentPermissions: Array<string>) => async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<void> => {
  let accessRules

  // FIXME: stabbed security; implement security
  if (req.header('Authorization') === 'non-secret-access') {
    accessRules = new GrandAccessACS()
  } else {
    throw new ServerError('Access denied', HttpStatus.FORBIDDEN)
  }

  req.accessRules = accessRules
  next()
}
