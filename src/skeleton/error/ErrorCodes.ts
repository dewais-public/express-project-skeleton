export enum ErrorCodes {
  NOTIFICATION_SENDING = 521,
  BATCH_SIZE_ERROR = 522,
}
