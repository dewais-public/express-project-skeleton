import { QueryBuilder } from 'knex'

import { ApplicationError } from '../../error/ApplicationError'
import { ValidationErrorCodes } from '../../error/DetailErrorCodes'
import { ServerError } from '../../error/ServerError'
import { logger } from '../../logger/LoggerFactory'
import { EntityFilter } from '../model/EntityFilter'
import { PaginationMetadata } from '../model/PaginationMetadata'

export class PaginationUtility {
  private static async totalCount(query: QueryBuilder): Promise<number> {
    const entityCountResult: Record<string, number> | undefined = await query
      .clone()
      .count<Record<string, number>>('uid')
      .first()

    if (!entityCountResult) {
      logger.debug('pagination.calculate.error.query')
      throw new ApplicationError('Count query returned undefined. This should never really happen.')
    }

    return +entityCountResult.count
  }

  public static async calculatePaginationMetadata(
    queryBuilder: QueryBuilder,
    filter: EntityFilter,
  ): Promise<PaginationMetadata> {
    logger.debug('pagination.calculate.start')
    const { limit, offset } = filter

    const total = await this.totalCount(queryBuilder)

    if (offset > total) {
      logger.debug('pagination.calculate.error.offset')
      throw new ServerError(
        'Offset bigger than total rows count',
        400,
        ValidationErrorCodes.FIELD_NUMBER_MAX_VALIDATION_ERROR,
        'pagination',
      )
    }
    logger.debug('pagination.calculate.done')

    return {
      limit,
      offset,
      total,
    }
  }

  public static applyPaginationForQuery(
    queryBuilder: QueryBuilder,
    filter: EntityFilter,
  ): QueryBuilder {
    logger.debug('pagination-utility.applyPaginationForQuery.start')

    if (filter.order) {
      const orderByDir = filter.order.asc ? 'asc' : 'desc'
      queryBuilder.orderBy(filter.order.orderBy, orderByDir)
    }

    logger.debug('pagination-utility.applyPaginationForQuery.done')
    return queryBuilder.limit(filter.limit).offset(filter.offset)
  }
}
