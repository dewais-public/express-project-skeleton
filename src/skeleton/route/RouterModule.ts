import { container } from 'tsyringe'

import { logger } from '../logger/LoggerFactory'
import { UserController } from './controller/user/UserController'

export class RouterModule {
  static async initialize(): Promise<void> {
    container.registerSingleton('UserController', UserController)

    logger.debug('app.context.router.module.initialized')
  }
}
