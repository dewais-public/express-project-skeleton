import express from 'express'
import { List } from 'immutable'
import { container } from 'tsyringe'

import { Controller } from './controller/Controller'

export class Router {
  private readonly controllers: List<Controller>

  constructor() {
    this.controllers = List<Controller>([container.resolve<Controller>('UserController')])
  }

  public mountRoutes(app: express.Application): void {
    const router = express.Router()
    router.get('/', (req: express.Request, res: express.Response) => {
      res.json({
        message: 'Express Server Skeleton!',
      })
    })
    app.use('/', router)

    this.controllers.forEach((c: Controller) => {
      const router = express.Router()
      c.initialize(router)
      app.use(c.path(), router)
    })
  }
}
