import { Request } from 'express'

import { ModelConstructor } from '../../../common/ModelConstructor'
import { User } from '../../../user/model/User'

export class UserModelConstructor implements ModelConstructor<User, User> {
  public constructRawForm(req: Request): User {
    return {
      uid: req.params.userId,
      username: req.body.username,
      password: req.body.password,
      // FIXME: implement security
      role: 'user',
    }
  }

  public constructPureObject(req: Request): User {
    return this.constructRawForm(req)
  }
}
