import { Request, Response, Router } from 'express'
import { inject, injectable } from 'tsyringe'

import { validate } from '../../../common/validators/ValidationMiddleware'
import { FilterValidator } from '../../../generic/validator/FilterValidator'
import { logger } from '../../../logger/LoggerFactory'
import { verifyAccess } from '../../../security/acs/ACSMiddleware'
import { GrandAccessACS } from '../../../security/acs/strategies'
import { UserService } from '../../../user/service/UserService'
import { UserValidator } from '../../../user/validator/UserValidator'
import { Controller } from '../Controller'
import { FilterModelConstructor } from '../FilterModelConstructor'
import { UserModelConstructor } from './UserModelConstructor'

@injectable()
export class UserController implements Controller {
  private modelConstructor: UserModelConstructor = new UserModelConstructor()
  private filterConstructor: FilterModelConstructor = new FilterModelConstructor()
  private filterValidator: FilterValidator = new FilterValidator()
  private validator: UserValidator = new UserValidator()

  constructor(
    @inject('UserService')
    private userService: UserService,
  ) {}

  public path(): string {
    return '/users'
  }

  public initialize(router: Router): void {
    router.get(
      '/',
      verifyAccess(),
      validate(this.filterConstructor, this.filterValidator),
      this.listUsers,
    )
    router.post(
      '/',
      verifyAccess(),
      validate(this.modelConstructor, this.validator),
      this.createUser,
    )
    router.get('/:userId', verifyAccess(), this.loadUser)
    router.put('/:userId', verifyAccess(), this.updateUser)
    router.delete('/:userId', verifyAccess(), this.deleteUser)
  }

  public listUsers = async (request: Request, response: Response): Promise<void> => {
    logger.debug('user.controller.list.start')

    const meta = this.filterConstructor.constructPureObject(request)

    const users = await this.userService.list(meta, new GrandAccessACS())
    response.json(users)
    logger.debug('user.controller.list.done')
  }

  public createUser = async (request: Request, response: Response): Promise<void> => {
    logger.debug('user.controller.create.start')
    const user = this.modelConstructor.constructPureObject(request)

    const uid = await this.userService.save(user, new GrandAccessACS())
    response.status(201).json({ uid })
    logger.debug('user.controller.create.done')
  }

  public loadUser = async (request: Request, response: Response): Promise<void> => {
    logger.debug('user.controller.load.start')
    const user = await this.userService.get(request.params.userId, new GrandAccessACS())
    if (user) {
      response.json(user)
    } else {
      response.status(404).send()
    }
    logger.debug('user.controller.load.done')
  }

  public updateUser = async (request: Request, response: Response): Promise<void> => {
    logger.debug('user.controller.update.start')
    const user = this.modelConstructor.constructPureObject(request)

    await this.userService.save(user, new GrandAccessACS())
    response.status(204).send()
    logger.debug('user.controller.update.done')
  }

  public deleteUser = async (request: Request, response: Response): Promise<void> => {
    logger.debug('user.controller.delete.start')
    await this.userService.delete(request.params.userId, new GrandAccessACS())

    response.status(204).send()
    logger.debug('user.controller.delete.done')
  }
}
