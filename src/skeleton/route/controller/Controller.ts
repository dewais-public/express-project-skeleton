import { Router } from 'express'

export interface Controller {
  path(): string

  initialize(router: Router): void
}
