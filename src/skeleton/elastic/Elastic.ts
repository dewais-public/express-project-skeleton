import { Client } from '@elastic/elasticsearch'

import { EnvironmentMode } from '../common/EnvironmentMode'
export class Elastic {
  private client: Client

  constructor() {
    const url = EnvironmentMode.isTest() ? process.env.ELASTIC_HOST_TEST : process.env.ELASTIC_HOST
    this.client = new Client({ node: url || 'http://localhost:9200' })
  }

  public async index<T>(id: string, index: string, body: T): Promise<void> {
    await this.client.index({ id, index, body })
  }

  public async search<T>(index: string, query?: {}): Promise<Array<T>> {
    try {
      const body = query ? { query } : {}
      const res = await this.client.search({ index, body })
      return res.body.hits.hits
    } catch (e) {
      if (e.meta.statusCode === 404) {
        return []
      }
      throw e
    }
  }

  public async delete(id: string, index: string): Promise<number> {
    try {
      await this.client.delete({ id, index })
      return 1
    } catch (e) {
      if (e.meta.statusCode === 404) {
        return 0
      }
      throw e
    }
  }

  public async clearAll(): Promise<void> {
    await this.client.indices.delete({ index: '_all' })
  }
}
