import { DateTime, DurationObjectUnits, DurationUnit } from 'luxon'

export class DateUtility {
  public static now(): Date {
    return DateTime.local()
      .toUTC()
      .toJSDate()
  }

  public static fromISO(dateISO: string): Date {
    return DateTime.fromISO(dateISO)
      .toUTC()
      .toJSDate()
  }

  public static getDateDiff(
    start: Date,
    end: Date,
    durationUnit: Array<DurationUnit>,
  ): DurationObjectUnits {
    return DateTime.fromJSDate(end)
      .toUTC()
      .diff(DateTime.fromJSDate(start).toUTC(), durationUnit)
      .toObject()
  }
}
