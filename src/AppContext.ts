import { DBModule } from './skeleton/db/DBModule'
import { ElasticModule } from './skeleton/elastic/ElasticModule'
import { logger } from './skeleton/logger/LoggerFactory'
import { LoggerModule } from './skeleton/logger/LoggerModule'
import { RouterModule } from './skeleton/route/RouterModule'
import { UserModule } from './skeleton/user/UserModule'

export class AppContext {
  static async initialize(): Promise<void> {
    await LoggerModule.initialize()
    await DBModule.initialize()
    await ElasticModule.initialize()
    await UserModule.initialize()
    await RouterModule.initialize()
    logger.info('app.context.initialized')
  }
}
