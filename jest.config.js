// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

const data = {
  preset: 'ts-jest',
  coverageDirectory: 'coverage',
  testEnvironment: 'node',

  roots: ['<rootDir>/test/unit', '<rootDir>/test/i9n'],
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
  testRegex: '(.*.spec)\\.ts$',
  moduleFileExtensions: ['ts', 'js', 'json', 'node'],

  coveragePathIgnorePatterns: [
    '<rootDir>/src/skeleton/elastic',
    '<rootDir>/src/skeleton/logger',
    '<rootDir>/src/skeleton/generic/dao',
    '<rootDir>/src/skeleton/generic/service',
    '<rootDir>/src/skeleton/security/acs/strategies',
    '<rootDir>/src/skeleton/db',
    '<rootDir>/config',
    '<rootDir>/database',
    '<rootDir>/test',
  ],
  coverageThreshold: {
    global: {
      branches: 70,
      functions: 70,
      lines: 70,
      statements: 70,
    },
  },
}

const getConfig = (mode = 'full') => {
  if (mode === 'unit') {
    data.roots = ['<rootDir>/test/unit']
  }

  if (mode === 'i9n') {
    data.roots = ['<rootDir>/test/i9n']
  }

  return data
}

module.exports = getConfig(process.env.TEST_MODE)
