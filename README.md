# Express skeleton
- Express
- Knex
- PostgreSQL

## Before start
`docker` and `docker-compose` should to be installed

Knex requires Node.js version 12.0 or newer.
Check your Node.js version using `node -v`, and if your version lower, than 12.0 update it.

## Run in development
Run within terminal script `run-services.sh` to launch databases.

Actualize database, if needed.  See below.

Start server:
```shell script
npm start
```

## Actualize database
When database is up an running (launch PostgreSQL or launch docker)
```shell script
npm run migrate:latest
```

## Tests
- Apply `npm test` for unit tests
- Apply `npm run test:i9n` for integration tests
- Add `:coverage` to check tests coverage (Look at package.json)

## Manual tests
[Postman](https://www.getpostman.com/downloads/) is a nice tool for testing REST API.
 
## More documentation
See folder `./docs/`
