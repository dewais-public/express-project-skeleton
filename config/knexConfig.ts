import 'dotenv/config'

export const config = {
  development: {
    client: 'pg',
    connection: {
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      port: +(process.env.DB_PORT || 5432),
    },
    migrations: {
      directory: __dirname + '/../database/migrations',
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: __dirname + '/../database/seeds',
    },
    log: {},
  },

  test: {
    client: 'pg',
    connection: {
      host: process.env.DB_HOST_I9N,
      user: 'postgres',
      password: 'pass4test',
      database: 'express_db_test',
      port: +(process.env.DB_PORT_i9N || 2345),
    },
    migrations: {
      directory: __dirname + '/../database/migrations',
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: __dirname + '/../test/database/seeds',
    },
    log: {},

    pool: {
      min: 2,
      max: 10,
    },
  },

  production: {
    client: 'pg',
    connection: {
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
    },
    pool: {
      min: 10,
      max: 100,
    },
    migrations: {
      directory: __dirname + '/../database/migrations',
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: __dirname + '/../database/seeds',
    },
    log: {},
  },
}
