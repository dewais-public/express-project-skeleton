import * as Knex from 'knex'

import { User } from '../../src/skeleton/user/model/User'

export const initialUserData: Array<User> = [
  {
    uid: '00000000-aaaa-aaaa-aaaa-000000000001',
    username: 'alpha.admin@gmail.com',
    password: 'temp4now!',
    role: 'superuser',
  },
  {
    uid: '00000000-aaaa-aaaa-aaaa-000000000002',
    username: 'beta.user@gmail.com',
    password: 'temp4now!',
    role: 'user',
  },
  {
    uid: '00000000-aaaa-aaaa-aaaa-000000000003',
    username: 'gamma.user@gmail.com',
    password: 'temp4now!',
    role: 'user',
  },
]

export async function seed(knex: Knex): Promise<void> {
  await knex('user').insert(initialUserData)
}
