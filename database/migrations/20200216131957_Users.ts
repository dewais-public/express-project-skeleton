import * as Knex from 'knex'

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('user', (table: Knex.TableBuilder) => {
    table.uuid('uid').primary()
    table
      .string('username', 128)
      .notNullable()
      .unique()
      .index('username')
    table.string('password', 128)
    table
      .string('role', 32)
      .notNullable()
      .defaultTo('user')
    table.dateTime('createdAt')
    table.dateTime('deletedAt').nullable()
    table.dateTime('lastLoginAt').nullable()
  })

  await knex.seed.run({ specific: '01_InitialData.ts' })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('user')
}
